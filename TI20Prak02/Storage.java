import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class Storage {
	
	//Name of the program
	private String progName;
	
	// List of urm statements
	private List<String> statements;
	
	// The key is the original variable and the value is the urm variable
	private Map<String, String> varMapping;
	
	// Contains non urm variables
	private List<String> inputVars;
	
	// Contains non urm variable
	private String outVar;
	
	// Contains non urm variables
	private List<String> helperVars;
	
	// Saves how many goto were used
	private int gotoCounter;
	
	public Storage() {
		statements = new ArrayList<>();
		varMapping = new HashMap<>();
		inputVars = new ArrayList<>();
		helperVars = new ArrayList<>();
		gotoCounter = 0;
	}
	
	public void setProgName(String progName) {
		this.progName = progName;
	}
	

	public String addVarAtEndOfMap(String key) {
		if (this.varMapping.containsKey(key)) {
			throw new DuplicateVariableException(key);
		}
		
		String urmVar = "R" + (this.varMapping.size() + 1);
		this.varMapping.put(key, urmVar);
		return urmVar;
	}


	public void addHelperVarExisting(String var) {
		addVarAtEndOfMap(var);
		helperVars.add(var);
	}
	
	/**
	 * Returns the urm mapping to var
	 * var needs to be a free non urm variable
	 * This should be used only internally e.g. for makro expansion in order to get a free variable
	 */
	public Pair addHelperVarInternalAndGetUrmVar(String var) {
		// Find a free variable
		int counter = 0;
		String newVar = var;
		while (varMapping.containsKey(newVar)) {
			newVar = var + counter;
			counter++;
		}

		helperVars.add(newVar);
		return new Pair(newVar, addVarAtEndOfMap(newVar));
	}
	
	public void setOutVarAndAddToMap(String outVar) {
		if (!this.varMapping.containsKey(outVar)) {
			this.varMapping.put(outVar, String.format("R%d", (this.varMapping.size() + 1)));
		}
		
		this.outVar = outVar;
	}
	
	
	public void addInputVar(String inVar) {
		if (inputVars.contains(inVar)) {
			throw new DuplicateVariableException(inVar);
		}
		inputVars.add(inVar);
		addVarAtEndOfMap(inVar);
	}
	
	
	public int getGotoCounter() {
		return gotoCounter;
	}
	
	
	/**
	 * Small helper method to check if a variable was defined in ether the input variables or the helper
	 * variables (given or introduced by this program)
	 * It throws a VariableNotDefinedException if the given variable has no mapping
	 */
	public String getUrmMappingFromVar(String var) {
		if (varMapping.containsKey(var) ) {
			return varMapping.get(var);
		} else {
			throw new VariableNotDefinedException(var);
		}
	}
	
	
	
	// =============== Statement section ====================
	
	public void addAssignZero(String var) {
		statements.add(String.format("%s = 0", getUrmMappingFromVar(var)));
	}
	
	
	public void addAssigntAddOne(String var1, String var2) {
		if (var1.equals(var2)) {
			statements.add(String.format("%s++", getUrmMappingFromVar(var1)));
		} else {
			addCopy(var1, var2);
			statements.add(String.format("%s++", getUrmMappingFromVar(var1)));
		}
	}
	
//	public void addGoto() {
//		statements.add(String.format("goto gotolabel%d", gotoCounter++));
//	}
	
	public void addGoto(int gotoCounter) {
		statements.add(String.format("goto gotolabel%d", gotoCounter));
	}
	
	public void addLabel(int gotoCounter) {
		statements.add(String.format("gotolabel%d", gotoCounter));
	}
	
	
	/**
	 * Adds the statements to copy the value from var2 to var1 while keeping the value in var1
	 * var1 and var2 are non urm variables which will be mapped to the corresponding urm variables
	 */
	public void addCopy(String var1, String var2) {
		Pair currentZPair = addHelperVarInternalAndGetUrmVar("z");
		
		int indexGotoLine2 = gotoCounter++;
		int indexGotoLine6 = gotoCounter++;
		int indexGotoLine11 = gotoCounter++;

		//COPY(m, A)
		statements.add(String.format("%s = 0", getUrmMappingFromVar(var1)));
		addLabel(indexGotoLine2);
		statements.add(String.format("if %s == 0 goto gotolabel%d", getUrmMappingFromVar(var2), indexGotoLine6));
		statements.add(String.format("%s--", getUrmMappingFromVar(var2)));
		statements.add(String.format("%s++", currentZPair.urmVar));
		statements.add(String.format("goto gotolabel%d", indexGotoLine2));
		addLabel(indexGotoLine6);
		statements.add(String.format("if %s == 0 goto gotolabel%d", currentZPair.urmVar, indexGotoLine11));
		statements.add(String.format("%s--", currentZPair.urmVar));
		statements.add(String.format("%s++", getUrmMappingFromVar(var1)));
		statements.add(String.format("%s++", getUrmMappingFromVar(var2)));
		statements.add(String.format("goto gotolabel%d", indexGotoLine6));
		addLabel(indexGotoLine11);
	}
	
	
	/**
	 * Adds the statements to transform the while statement into urm statements
	 * var1 and var2 are non urm variables which will be mapped to the corresponding urm variables
	 */
	public void addWhile(String var1, String var2, int indexBeforeWhile) {
		Pair currentMPair = addHelperVarInternalAndGetUrmVar("m");
		Pair currentNPair = addHelperVarInternalAndGetUrmVar("n");
		
		// addCopy uses the non urm variables and mappes them
		addCopy(currentMPair.normalVar, var1);
		addCopy(currentNPair.normalVar, var2);
		
		Pair currentZPair = addHelperVarInternalAndGetUrmVar("z");
		
		int indexGotoLine1 = (gotoCounter-1);
		gotoCounter++;
		int indexGotoLine6 = gotoCounter++;
		int indexGotoLine9 = gotoCounter++;
		int indexGotoLine11 = gotoCounter++;
		int indexGotoAfterWhile = gotoCounter++;
		
		addLabel(indexGotoLine1);
		statements.add(String.format("if %s == 0 goto gotolabel%d", currentMPair.urmVar, indexGotoLine9));
		statements.add(String.format("if %s == 0 goto gotolabel%d", currentNPair.urmVar, indexGotoLine6));
		statements.add(String.format("%s--", currentMPair.urmVar));
		statements.add(String.format("%s--", currentNPair.urmVar));
		statements.add(String.format("goto gotolabel%d", indexGotoLine1));
		addLabel(indexGotoLine6);
		statements.add(String.format("%s = 0", currentZPair.urmVar));
		statements.add(String.format("%s++", currentZPair.urmVar));
		
		statements.add(String.format("goto gotolabel%d", indexGotoLine11));
		addLabel(indexGotoLine9);
		statements.add(String.format("if %s != 0 goto gotolabel%d", currentNPair.urmVar, indexGotoLine6));
		
		statements.add(String.format("%s = 0", currentZPair.urmVar));
		addLabel(indexGotoLine11);
		statements.add(String.format("if %s == 0 goto gotolabel%d", currentZPair.urmVar, indexGotoAfterWhile));
	}
	
	
	/**
	 * Until now the goto uses the string gotolabel<number> as reference. Now this will be replaced by the correct linenumber
	 * The for loop iterates all statements and when a statement matches only gotolabel<NUMBER> another for loop will
	 * look for all occurences of that label. Also the gotolabel line will be removed as it is not a valid URM statement
	 */
	private void replaceLabelsWithLineNumbers() {
		for (int i = 0; i < statements.size(); i++) {
			// Match only lines which consist of the string gotolabel<NUMBER>
			if (statements.get(i).matches("^gotolabel(\\d+)$")) {
				for (int j = 0; j < statements.size(); j++) {
					if (i != j) {
						// We need to use endsWith so that gotolabe1 does not replace gotolabel10
						if (statements.get(j).endsWith(statements.get(i))) {
							statements.set(j, statements.get(j).replaceAll(statements.get(i), Integer.toString(i + 1)));
						}
					}
				}
				statements.remove(i);
				i--;
			}
		}
	}
	
	
	/**
	 * It is possible that the same two labels will be added as statements.
	 * This could be fixed in code but I think that solution would increase the complexety of the code even more
	 * and some parts would be harder to understand
	 *
	 * So this method is iterating the statements and deleting the lines that have duplicate gotolabels
	 */
	private void removeDuplicatedGotoLabels() {
		for (int i = 0; i < statements.size()-1; i++) {
			if (statements.get(i).matches("^gotolabel(\\d+)$")
					&& statements.get(i+1).matches("^gotolabel(\\d+)$")
					&& statements.get(i).equals(statements.get(i+1))) {
				statements.remove(i);
				i--;
			}
		}
		
	}
	
	
	public void saveUrmProg() {
		System.out.println("The name of the program is: " + progName);
		System.out.println("Writing the urm program to: urm/" + progName + ".urm");
		removeDuplicatedGotoLabels();
		replaceLabelsWithLineNumbers();
		
		String filename = progName + ".urm";
		
		StringBuilder sb = new StringBuilder();
		sb.append("; ").append(progName);
		sb.append("(in ");
		sb.append(Arrays.toString(inputVars.stream().map(s -> varMapping.get(s)).toArray()));
		sb.append("; out ");
		sb.append(varMapping.get(outVar));
		sb.append(")\n");
		
		// Print statements
		for (int i = 0; i < statements.size(); i++) {
			if (i != 0) {
				sb.append("\n");
			}
			sb.append(statements.get(i));
		}
		//Empty line at the end
		sb.append("\n\n");
		
		try {
			// Writing the output to a file
			Path pUrmDir = Paths.get("urm");
			if (!Files.isDirectory(pUrmDir)) {
				Files.createDirectory(pUrmDir);
			}
			Path pUrm = pUrmDir.resolve(filename);
			Files.write(pUrm, sb.toString().getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Small helper class that saves the mapping of a variable to its urm counterpart
	 */
	static class Pair {
		String normalVar;
		String urmVar;
		public Pair(String normalVar, String urmVar) {
			this.normalVar = normalVar;
			this.urmVar = urmVar;
		}
	}
	
	
	static class DuplicateVariableException extends RuntimeException {
		public DuplicateVariableException(String varname) {
			super(String.format("Duplicate variable: %s", varname));
		}
	}
	
	static class VariableNotDefinedException extends RuntimeException {
		public VariableNotDefinedException(String varname) {
			super(String.format("Variable was not defined: %s", varname));
		}
	}
}