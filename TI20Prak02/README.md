# TI - while0 to urm compiler
This is a program that translates while0 programs to urm.

## Requirements
To compile the program you need javacc and java 8 or higher.

## Compiling
The easiest way to compile it, is by entering the command `make` if you are using Linux or MacOS.
To recompile the program simply execute `make clean` and than `make` again.

Otherwise execute the following commands:
```
javacc Praktikum02.jj
javac *.java
```

## Usage
In order to compile a while0 program execute the following command `java Praktikum02 <PathToWhile0File>`. This will create a folder named `urm` in the current directory, if it does not exists jet, and save the urm file there.
**Note: The urm filename will be determined by the while0 program name, not by the while0 filename**

## Tests
In the `while0_progs` folder are a number of while0 programs, not all of which are valid.
These are used to test the compiler.
In order to run the compiler with all files in `while0_progs`, just execute `make test`.
The last four programs should result in error messages (`add_no_semicolons.while0`, `semicolon_after_last_exp.while0`, `wrong_order.while0` and `duplicate_var.while0`)
