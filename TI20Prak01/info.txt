Ich musste zwei Dinge aus Praktikum 0 ändern, da diese fehlerhaft waren:
- In der Zeile <IDENT: ["a"-"z","A"-"Z"] (["a"-"z","A"-"Z","0"-"9"])* > habe ich bei dem ersten Buchstabenbereich vergessen die einzelnen Buchstaben in doppelte Anführungszeichen zu setzten.
- Die EBNF Regel für exp = (assignExp | whileExp), {";", exp};  habe ich so umgesetzt, dass der Teil {";", exp} nicht beliebig häufig ersetzt werden kann sondern optional einmalig ist, also [";", exp]. Grund hierfür ist, dass immernoch die selbe Sprache erzeugt wird, die Sprache aber mit einem look ahead von 1 auskommt
